package cl.acaya.api.business;

/**
 * Created by mcastro on 22-05-14.
 */
public interface BusinessParameter {

    String DB_SCHEMA = "dbo";
    String RUT_CLIENTE = "rutCliente";
    String SOCIEDAD = "sociedad";
    String IP_SAP = "10.1.24.56";
    String NAME_SAP = "PRD";
    String CLIENTE_SAP = "300";
    String SYSTEM_NUMBER = "0";
    String USER_SAP = "SPKII";
    String PASSWORD_SAP = "acero2013";
    String RFC_DATOSCLIENTE = "ZSDFN_DATOS_CLIENTE_11";
    String RFC_CARGOS_SAP = "ZFIFN_SCKFUNCION";
}
